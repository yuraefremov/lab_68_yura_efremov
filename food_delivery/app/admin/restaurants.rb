ActiveAdmin.register Restaurant do

  form do |f|
    f.inputs do
      f.input :name
      f.input :description
      if f.object.picture.attached?
        f.input :picture,
        :as => :file,
        :hint => image_tag(
          url_for(
            f.object.picture.variant(combine_options: { gravity: 'Center', crop: '50x50+0+0' })
            )
          )
      else
       f.input :picture, :as => :file
     end
   end
   f.actions
 end

 index do
  selectable_column
  id_column
  column :picture do |restaurant|
    image_tag restaurant.picture.variant(combine_options: { gravity: 'Center', crop: '50x50+0+0' })
  end
  column :name do |restaurant|
    link_to restaurant.name, admin_restaurant_path(restaurant)
  end
  actions
end

show do
 attributes_table do
   row :picture do |restaurant|
    image_tag restaurant.picture.variant(combine_options: { gravity: 'Center', crop: '50x50+0+0' })
  end
  row :name
  row :description
end
active_admin_comments
end


permit_params :name, :description, :picture

end

