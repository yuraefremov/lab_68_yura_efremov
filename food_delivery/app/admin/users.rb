ActiveAdmin.register User do
  form do |f|
    f.inputs do
      f.input :name
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :phone
      f.input :address
      f.actions
    end
 end
    index do
      selectable_column
      id_column
      column :name 
      column :email
      column :phone
      column :address

      actions
    end

    show do
     attributes_table do
       row :name 
       row :email
       row :phone
       row :address
     end
     active_admin_comments
   end



   permit_params :name,:email, :phone, :address, :password, :password_confirmation

end


