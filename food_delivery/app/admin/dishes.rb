ActiveAdmin.register Dish do
  form do |f|
    f.inputs do

      f.input :name
      f.input :description
      f.input :price
      if f.object.picture.attached?
        f.input :picture,
        :as => :file,
        :hint => image_tag(
          url_for(
            f.object.picture.variant(combine_options: { gravity: 'Center', crop: '50x50+0+0' })
            )
          )
      else
       f.input :picture, :as => :file
     end
     f.input :restaurant_id, :label => 'Restaurants', :as => :select, :collection => Restaurant.all.map{|u| [u.name, u.id]}
   end
   f.actions
 end

 index do
  selectable_column
  id_column
  column :picture do |dish|
    if dish.picture.attached?
    image_tag dish.picture.variant(combine_options: { gravity: 'Center', crop: '50x50+0+0' })
    end
   end 
  column :name do |dish|
    link_to dish.name, admin_dish_path(dish)
  end
  column :description
  column :price
  column :restaurant_id
  actions
end

show do
  attributes_table do

    row :picture do |dish|
      if dish.picture.attached?
        image_tag dish.picture.variant(combine_options: { gravity: 'Center', crop: '200x200+0+0' })
      end
    end
  row :name
  row :description
  row :price
  row :restaurant
  end
active_admin_comments
end


permit_params :name, :description, :price, :picture, :restaurant_id

end


