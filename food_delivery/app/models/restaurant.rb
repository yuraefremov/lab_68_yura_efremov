class Restaurant < ApplicationRecord
  has_one_attached :picture
  has_many :dishes, dependent: :destroy
  has_many :line_items, dependent: :destroy

  validates :name, presence: true,
  length: { minimum: 2, maximum: 20 }


  validates :description, presence: true,
  length: { minimum: 10, maximum: 500 }


  validates :picture,  file_content_type: { allow: ['image/jpeg', 'image/gif', 'image/png'] }

end
