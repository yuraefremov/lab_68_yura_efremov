class LineItem < ApplicationRecord
  belongs_to :dish
  belongs_to :cart
  belongs_to :user
  belongs_to :restaurant
  
  def total_price
    self.quantity * self.dish.price
  end

end
