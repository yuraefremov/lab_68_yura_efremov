class Dish < ApplicationRecord
  has_one_attached :picture
  belongs_to :restaurant
  has_many :line_items, dependent: :destroy
  
  validates :name, presence: true,
  length: { minimum: 3, maximum: 20 }


  validates :description, presence: true,
  length: { minimum: 10, maximum: 500 }

  validates :price, presence: true,
  numericality: { greater_than_or_equal_to: 0 }


  validates :picture,  file_content_type: { allow: ['image/jpeg', 'image/gif', 'image/png'] }


end
