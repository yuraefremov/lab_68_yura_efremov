Rails.application.routes.draw do
 
  # get 'carts/show'
  ActiveAdmin.routes(self)
  devise_for :users
  root 'restaurants#index'

  get 'carts/:id' => "carts#show", as: "cart"
  delete 'carts/:id' => "carts#destroy"

  post 'line_items/:id/add' => "line_items#add_quantity", as: "line_item_add"
  post 'line_items/:id/reduce' => "line_items#reduce_quantity", as: "line_item_reduce"
  post 'line_items/:dish_id' => "line_items#create", as: 'line_items'
  get 'line_items/:id' => "line_items#show", as: "line_item"
  delete 'line_items/:id' => "line_items#destroy"

  resources :dishes
  resources :restaurants

end
